// MARK: - Protocol of Component
protocol Component {
    func add(component: Component)
    func operation() -> String
}

extension Component {
    func add(component: Component) {}
}

protocol Builder {
    func buildFraction(an: [Int], bn: [Int]?)
    func getFraction() -> Component
}


class Leaf: Component {
    var parent: Component?
    var number: Int
    
    init(_ number: Int) {
        self.number = number
    }
    
    func operation() -> String {
        return String(number)
    }
}

class Composite: Component {
    private var children = [Component]()
    var B: Int
    
    init(_ B: Int = 1) {
        self.B = B
    }
    
    func add(component: Component) {
        children.append(component)
    }
    
    func operation() -> String {
        let result = children.map({ $0.operation() })
        
        return "(" + result.joined(separator: " + \(String(describing: B))/") + ")"
    }
}

class Fraction1Builder: Builder {
    private var fraction = Composite()
    
    func buildFraction(an: [Int], bn: [Int]?) {
        var nextBranch = fraction
        
        for i in 0...an.count - 1 {
            if i < an.count - 2 {
                nextBranch.add(component: Leaf(an[i]))
                let branch = Composite()
                nextBranch.add(component: branch)
                nextBranch = branch
            } else {
                nextBranch.add(component: Leaf(an[i]))
            }
        }
    }
    
    func getFraction() -> Component {
        return fraction
    }
}

class Fraction2Builder: Builder {
    private var fraction = Composite()
    
    func buildFraction(an: [Int], bn: [Int]?) {
        fraction.B = bn![0]
        var nextBranch = fraction
        
        for i in 0...an.count - 1 {
            if i < an.count - 2 {
                nextBranch.add(component: Leaf(an[i]))
                let branch = Composite(bn![i+1])
                nextBranch.add(component: branch)
                nextBranch = branch
            } else {
                nextBranch.add(component: Leaf(an[i]))
            }
        }
    }
    
    func getFraction() -> Component {
        return fraction
    }
}


let an: [Int] = [1, 2, 3, 4, 5]
let bn: [Int] = [6, 7, 8, 9]

var builder1: Builder = Fraction1Builder()
var builder2: Builder = Fraction2Builder()
var fraction: Component

builder1.buildFraction(an: an, bn: nil)
fraction = builder1.getFraction()
fraction.operation()

builder2.buildFraction(an: an, bn: bn)
fraction = builder2.getFraction()
fraction.operation()

